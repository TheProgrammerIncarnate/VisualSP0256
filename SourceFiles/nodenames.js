/*
 Copyright (c) 2018 Cole Johnson

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
//This file is A MESS, will clean it up sometime
var nodenames ={
//Critical nodes
gnd: 1,      // pads: ground
vdd: 126,       // pads: power
reset: 3918,   // pads: reset
clk: 3878,	//clock pin

pinStandByReset: 3698,
pinROMClock: 3783,
pinOsc2: 3797,
pinROMDisable: 3929,
pinOutControl1: 3930,
pinOutControl2: 3718,
pinOutControl3: 3216,
pinStandby: 2079,
pinLoadRequest: 1338,
pinAddress1: 875,
pinAddress2: 154,
pinAddress3: 17,
pinAddress4: 16,
pinAddress5: 15,
pinAddress6: 14,
pinAddress7: 13,
pinAddress8: 12,
pinSerialAddressOut: 11,
pinStrobeEnable: 141,
pinAddressLoad: 473,
pinTest: 1995,
pinDigitalOut: 3167,
pinSerialIn: 1582,

internal_reset: 78,
returnOrSetPageReg: 3649,
mode: 3664,
load1: 3691,
load2: 3701,
load3: 3704,
load4: 3709,
load5: 3711,
load6: 3717,
loadFull: 3730,
delta1: 3737,
load7: 3741,
delta2: 3747,
load8: 3760,
jumpIntoSub: 3769,
jumpToSpot: 3776,
pauseCommand: 3784,

decodeDisable: 2928,
NOT_decodeDisable: 2955,
firstDebug: 3894,
debug2: 706,
debug3: 387,
flipMe1: 3694,
flipMe2: 3725,
ctrlDebug1: 3888,
ctrlDebug2: 3890,
ctrlDebug3: 3892,
//Main control line signals
ctrlDebug1m: 150,
ctrlDebug2m: 149,
ctrlDebug3m: 148,
NOT_ctrlDebug1: 3936,
NOT_ctrlDebug2: 3937,
NOT_ctrlDebug3: 3938,

//4828 1655
ppatch1: 388,
//4872 1579
//IN HALF STEP HARD-CODED
ppatch2: 479,

pullMe: 3695,
//VD1 pin is treated as Vdd
}
