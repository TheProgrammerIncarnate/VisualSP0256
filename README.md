This is the JavaScript simulator for the AP0256 voice synthesis/decompression chip

This is a beta version, the chip does not yet execute internal opcodes correctly ;(

Based on the work of the visual6502 team (Their source is at https://github.com/trebonian/visual6502)
You can find more info at my blog (https://nerdstuffbycole.blogspot.com/)

Die images provided by Greg James and Christian Sattler.
Very big thanks to JohnPCAE for highlighting work.

See Credits file for more info.

Note the various licenses and Copyright associated with each file.
Unless otherwise noted, all code is under the MIT License

While you are not legally required to, it would be appreciated that, if you reproduce or develop this work, you;

	-Provide links to the original creators (Cole Johnson and the Visual6502 team) beyond those in the source
	-Notify me of any projects/uses the simulator has helped you with
	-Publish the source of any new versions so others can learn from and expand upon our work

Enjoy! -Cole Johnson

"Enjoy!" -The Visual6502 Team
