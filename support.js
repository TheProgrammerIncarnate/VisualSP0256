 /*
 Copyright (c) 2010 Ed Spittles
 Copyright (c) 2018 Cole Johnson

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/
/*
* support.js for the SP0256
* Contains chip-specific support functions and values
* These may override the defaults in macros.js
*/
chipname='GI SP0256';
statusLines = 5;
/*
* The canvas is 2000x2000
* The source image is 8500x8500 (if a blank area is added to make it square)
* A future update should allow larger chips to be rendered without graphical artifacts
*/
grChipSize=8500;
undoSize = 50;
ngnd = nodenames['gnd'];
npwr = nodenames['vdd'];

nodenamereset = 'reset';

//No advanced logging has been introduced for this chip, yet
presetLogLists=[];

const digWidth = 400;
const digHeight = 100;
var hPos = 0;
var vPos = 0;
var dCanvas;
var dCtx;

//sets up anything chip-specific
function setupSpecial()
{
	dCanvas = document.getElementById("digitalCanvas");
	dCtx = dCanvas.getContext("2d");
	dCanvas.backgroundColor = digitalBackgroundColor;
	dCanvas.width = digWidth;
	dCanvas.height = digHeight;
	dCtx.fillStyle = digitalBackgroundColor;
  	dCtx.fillRect(0,0,digWidth,digHeight);
	hPos = 0;
}

// simulate a single clock phase with no update to graphics or trace
function halfStep()
{
	var clk = isNodeHigh(nodenames['clk']);
	if (clk) {setLow('clk'); updateDigitalOut();} 
	else {setHigh('clk'); }
	nodes[479].pullup = isNodeHigh(nodenames['ppatch1']);
	/*if(!isNodeHigh(nodenames['pinOutControl2']) && !isNodeHigh(nodenames['pinOutControl3']) && isNodeHigh(nodenames['pinOutControl1']))
	{
		setLow(nodenames['debug3']);
	}
	else
	{
		setHigh(nodenames['debug3']);
	}*/
}
var lastState = false;
function updateDigitalOut()
{
	var state = isNodeHigh(nodenames['pinDigitalOut']);
	dCtx.fillStyle = digitalLineColor;
	if(state!=lastState)
	{
		dCtx.fillRect(hPos,20,1,61);
		lastState = state;
	}
	else if(state)
	{
		dCtx.fillRect(hPos,20,1,1);
	}
	else
	{
		dCtx.fillRect(hPos,80,1,1);
	}
	hPos++;
	if(hPos==400)
	{
		dCtx.fillStyle = digitalBackgroundColor;
  		dCtx.fillRect(0,0,digWidth,digHeight);
  		hPos = 0;
	}
}
	/*function updateDOut()
{
	var drawColor = digitalLow;
	if(isNodeHigh(nodenames['pinDigitalOut']))
		drawColor = digitalHigh;
	dCtx.fillStyle = drawColor;
	dCtx.fillRect(hPos * 2,vPos * 2,2,2);
	hPos++;
	if(hPos==200)
	{
		hPos = 0;
		vPos++;
		if(vPos==50)
		{
			vPos = 0;
		}
		dCtx.fillStyle = digitalBackgroundColor;
  		dCtx.fillRect(0,vPos * 2,digWidth,2);
	}*/
/*
* Initializes the chips state
* First it turns all nodes (except power) and transistors off
* Sets various nodes such as pins to correct values with initNodeValues()
* then runs a full update
*/
function initChip()
{
   var start = now();
	for(var nn in nodes)
	{
		//Apparently the float property is unused
		nodes[nn].state = false;
		nodes[nn].float = true;
	}

	nodes[ngnd].state = false;
	nodes[ngnd].float = false;
	nodes[npwr].state = true;
	nodes[npwr].float = false;
	for(var tn in transistors) transistors[tn].on = (transistors[tn].gate==npwr);//changed to power always powered transistors
	initNodeValues();
	//The following line does not appear to be needed. I do not know its original purpose in the 6502 sim
	//for(var i=0;i<6;i++){halfStep();} // avoid updating graphics and trace buffer before user code
	refresh();
	cycle = 0;
	farthestCycle = 0;
	trace = Array();
	if(typeof expertMode != "undefined")
		updateLogList();
	chipStatus();
	if(ctrace)console.log('initChip done after', now()-start);
}

/*
* Set any nodes, besides the ground and power ones, to default values
*/
function initNodeValues()
{
	setPd("pinStandByReset",false);
	setPd("reset",false);
	setPd("pinStrobeEnable",true);
	setPd("pullMe",false);
	//setPd("pinLoadRequest",true);
	setPd("pinAddressLoad",true);
	setPd("pinStandByReset",true);
	setPd("reset",true);
	recalcNodeList(allNodes());
}
/*
* Called by the toggle pin checkboxes
*/
function updateInputs(pinName, state)
{
	if(state)
	{
		setPd(pinName,true);
	}
	else
	{
		setPd(pinName,false);
	}
	recalcNodeList(allNodes()); 
	//Update the drawing, but only if the simulation is paused and graphics enabled
	if(!running)
		refresh();
}
function updateDebug(pinName, state)
{
	console.log("debug");
	//return;
	if(state)
	{
		setPd(pinName,true);
		setPd(pinName+"m",true);
		setPd("NOT_"+pinName,false);
	}
	else
	{
		setPd(pinName,false);
		setPd(pinName+"m",false);
		setPd("NOT_"+pinName,true);
	}
	recalcNodeList(allNodes()); 
	//Update the drawing, but only if the simulation is paused and graphics enabled
	if(!running)
		refresh();
}
//The only change between this and the original stepBack()
//is that we will call other functions sometime
function stepBack()
{
	var traceAt = --cycle;
	if(farthestCycle>=undoSize)
	{
		traceAt = undoSize - (farthestCycle - (cycle));
	}
	if(traceAt<0) return;
	currentState = trace[traceAt];
	showState(currentState.chip);
	chipStatus();
}
//Updates the status on the control panel
function chipStatus()
{
	var line1 = "Cycle: "+cycle+" "+"Speed: " + estimatedHz().toFixed(2) + " Hz";
	var line2 = chipStatusLine;
	var line3 = getStateInfo();
	var line4 = getSequencerInfo();
	setStatus(line1,line2,line3,line4);
}
var controlNames = ['NOP','Address SR Load','Address SR -> PC','Data SR Load','Data SR Shift','Stack Fill','Load from Stack','Reset'];
function getStateInfo()
{
	var output = "Running... ";
	if(isNodeHigh(nodenames['pinStandby']))
		output = "On Standby."
	if(isNodeHigh(nodenames['pinLoadRequest']))
		output = output + " Input Full";
	else
		output = output + " Input Open";
	output = output + ". Ctrl: "
	var val = 0;
	if(isNodeHigh(nodenames['pinOutControl3']))
		val = val + 1;
	if(isNodeHigh(nodenames['pinOutControl2']))
		val = val + 2;
	if(isNodeHigh(nodenames['pinOutControl1']))
		val = val + 4;
	return output + controlNames[val];
}
function getSequencerInfo()
{
   var output = "OPcode: ";
	if(isNodeHigh(nodenames['returnOrSetPageReg']))
		output = output + "R/S Page Register";
	else if(isNodeHigh(nodenames['mode']))
		output = output + "Mode Change";
	else if(isNodeHigh(nodenames['load1']))
		output = output + "Load #1";
	else if(isNodeHigh(nodenames['load2']))
		output = output + "Load #2";
	else if(isNodeHigh(nodenames['load3']))
		output = output + "Load #3";
	else if(isNodeHigh(nodenames['load4']))
		output = output + "Load #4"
	else if(isNodeHigh(nodenames['load5']))
		output = output + "Load #5";
	else if(isNodeHigh(nodenames['load6']))
		output = output + "Load #6";
	else if(isNodeHigh(nodenames['loadFull']))
		output = output + "Load All";
	else if(isNodeHigh(nodenames['delta1']))
		output = output + "Delta #1";
	else if(isNodeHigh(nodenames['load7']))
		output = output + "Load #7";
	else if(isNodeHigh(nodenames['delta2']))
		output = output + "Delta #2";
	else if(isNodeHigh(nodenames['load8']))
		output = output + "Load #8";
	else if(isNodeHigh(nodenames['jumpIntoSub']))
		output = output + "Subroutine Start";
	else if(isNodeHigh(nodenames['jumpToSpot']))
		output = output + "Jump";
	else if(isNodeHigh(nodenames['pause']))
		output = output + "Pause";
	return output
}
//Currently the chip does not save any of its info to URLs
function readParam(name,value)
{
	return true;
}
