 /*
 Copyright (c) 2010 Brian Silverman, Barry Silverman
 Copyright (c) 2018 Cole Johnson

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

/*
* render.js contains setup and runtime drawing routines for the visual chip simulation
* The simulation uses four different canvases on top of one another to draw the chip
* Background: Shows where nodes and transistors are located. Only redrawn when a layer is hidden/shown
* Overlay: Covers powered nodes (and thus transistors) with a semi-transparent white layer. Redrawn when refresh() is called
* Hilite: Covers selected nodes and transistors with highlighting based on state. Redrawn by refresh() and when the user highlights another node
* Hitbuffer: Invisible layer used to figure out what node the user clicked on. Only drawn on page load
* NOTE:
* Originally was wires.js
* A few support functions were moved to macros.js, making this entirely dedicated to rendering/hitbuffer
*/
var frame, chipbg, overlay, hilite, hitbuffer, ctx;

var chipLayoutIsVisible = true;  // only modified in expert mode
var hilited = [];

/*
* Sets the GUI's layer visibility checkboxes to the default states as specified
* by the initial state of drawlayers array
*/
function setupLayerVisibility()
{
	var x=document.getElementById('updateShow');
	for (var i=0;i<x.childNodes.length;i++) {
		if(x.childNodes[i].type='checkbox'){
			if(x.childNodes[i].name==-1)
			{
				x.childNodes[i].checked=drawTLayer			
			}			
			else
			{
				x.childNodes[i].checked=drawlayers[x.childNodes[i].name];
			}
		}
	}
}
//A list of all polygons in their draw order
var bDrawOrder = [];
/*
* Draws the nodes and transistors in the background layer
* called only on setup and if the user turns a background layer on/off
*/
function setupBackground()
{
	//If this is the first time setupBackground() is called, then we initilize the draw order array
	if(bDrawOrder.length==0)
	{
		//Create a clone of segdefs
		//We get info directly from segdefs because we are messing around with polygons, not nodes
		var toReorder = segdefs.slice(0)
		for(var a in backgroundDrawOrder)
		{
			//If the transistor layer is added next, we put in a dummy polygon which will
			//trigger the transistor layer draw function
			if(backgroundDrawOrder[a]==-1)
			{
				bDrawOrder.push([-1]);
				continue;
			}
			//Search for and add all polygons in the next layer
			//We run the loop backwards because we are removing elements from it
			for(var b=toReorder.length-1;b>=0;b--)
			{
				//TODO: Get rid of unnecessary values in reorder polygons
				//Is the node in the layer we are currently adding?
				if(toReorder[b][2]==backgroundDrawOrder[a])
				{
					bDrawOrder.push(toReorder[b]);
					toReorder.splice(b,1);
				}			
			}
		}
	}
	chipbg = document.getElementById('chipbg');
	chipbg.width = grCanvasSize;
	chipbg.height = grCanvasSize;
	var ctx = chipbg.getContext('2d');
	ctx.fillStyle = backgroundFill;
	ctx.strokeStyle = backgroundStroke;
	ctx.lineWidth = grLineWidth;
	ctx.fillRect(0,0,grCanvasSize,grCanvasSize);
	for(var i in bDrawOrder)
	{
		var seg = bDrawOrder[i];
		//If we find our dummy polygon, then we render the transistor layer
		if(seg[0]==-1)
		{
			if(drawTLayer)
			{
					ctx.fillStyle = transistorColor;
				for(var t in transdefs)
				{
					var tran = transdefs[t];
					var bb = tran[4];
					drawSeg(ctx, bb);
					ctx.fill();
				}
			}
			continue;
		}
		//If not a transistor, we render the segment based on what layer it's in
		var c = seg[2];
		if(drawlayers[c])
		{
			ctx.fillStyle = colors[c];
			drawSeg(ctx, bDrawOrder[i].slice(3));
			ctx.fill();
		}
	}
}

function setupOverlay()
{
	overlay = document.getElementById('overlay');
	overlay.width = grCanvasSize;
	overlay.height = grCanvasSize;
	ctx = overlay.getContext('2d');
}

function setupHilite()
{
	hilite = document.getElementById('hilite');
	hilite.width = grCanvasSize;
	hilite.height = grCanvasSize;
	var ctx = hilite.getContext('2d');
}

function setupHitBuffer()
{
	hitbuffer = document.getElementById('hitbuffer');
	hitbuffer.width = grCanvasSize;
	hitbuffer.height = grCanvasSize;
	hitbuffer.style.visibility = 'hidden';
	var ctx = hitbuffer.getContext('2d');
	for(i in nodes) hitBufferNode(ctx, i, nodes[i].segs);
}
/*
* We draw the node with <color> where color is the node's number
* converted into an RGB value
* When a pixel is clicked, we simply grab and decode the color to get our node #
* NOTE:
* Currently allows only 4096 different nodes (no transistors)
* expanding the number possible shouldn't be too hard
*/
function hitBufferNode(ctx, i, w)
{
	var low = hexdigit(i&0xf);
	var mid = hexdigit((i>>4)&0xf);
	var high = hexdigit((i>>8)&0xf);
	ctx.fillStyle = '#'+high+'F'+mid+'F'+low+'F';
	for(i in w) {
		drawSeg(ctx, w[i]);
		ctx.fill();
	}
}

function hexdigit(n){return '0123456789ABCDEF'.charAt(n);}


/////////////////////////
//
// Drawing Runtime
//
/////////////////////////

/*
* Redraws the overlay (based on nodes being on/off)
* as well as the highlight layer
* Does not redraw the background or hitbuffer
*/
function refresh()
{
	if(!chipLayoutIsVisible) return;
	ctx.clearRect(0,0,grCanvasSize,grCanvasSize);
	for(i in nodes)
	{
		if(isNodeHigh(i)) overlayNode(nodes[i].segs);
	}
	hiliteNode(hilited);
}

function overlayNode(w)
{
	ctx.fillStyle = overlayColor;
	for(i in w) {
		drawSeg(ctx, w[i]);
		ctx.fill();
	}
}

/*Highlights a node/list of nodes, colored according to logical state
* also highlights connected transistors according to logical state + connection type
*/
function hiliteNode(n)
{
	var ctx = hilite.getContext('2d');
	//clear the hilite layer
	ctx.clearRect(0,0,grCanvasSize,grCanvasSize);
	if(n==-1) return;
	hilited = n;
	for(var i in n)
	{
		var currentNode = nodes[n[i]];
		if(isNodeHigh(n[i]))
			ctx.fillStyle = poweredHilite;
		else
			ctx.fillStyle = unpoweredHilite;
		var segs = currentNode.segs;
		for(var s in segs){drawSeg(ctx, segs[s]); ctx.fill();}
		//in the case of multiple nodes selected, we only highlight the transistors of the first one
		if(i==0)//&n[i]!=ngnd)
		{
			//Overlay the output transistors with the output color/ possible transistor overlay
			for(var g in currentNode.gates)
			{
				ctx.fillStyle = outputTransistorColor;
				drawSeg(ctx,currentNode.gates[g].bb);
				ctx.fill();
				if(currentNode.gates[g].on)
				{
					ctx.fillStyle = transistorPoweredHilite;
					drawSeg(ctx,currentNode.gates[g].bb);
					ctx.fill();
				}
			}
			//Overlay the input transistors with the input color/ possible transistor overlay
			for(var c in currentNode.c1c2s)
			{
				ctx.fillStyle = inputTransistorColor;
				drawSeg(ctx,currentNode.c1c2s[c].bb);
				ctx.fill();
				if(currentNode.c1c2s[c].on)
				{
					ctx.fillStyle = transistorPoweredHilite;
					drawSeg(ctx,currentNode.c1c2s[c].bb);
					ctx.fill();
				}
			}
		}
	}
}

// highlight a single transistor (additively - does not clear highlighting)
/*
* Currently not used
*/
function hiliteTrans(n){
	var ctx = hilite.getContext('2d');
	ctx.strokeStyle = transistorHiliteStroke;
	ctx.lineWidth = 4;
	for(var t in n)
	{
		var bb = transistors[n[t]].bb
		drawSeg(ctx, bb); 
		ctx.stroke();
	}
}

function ctxDrawBox(ctx, xMin, yMin, xMax, yMax)
{
	var cap=ctx.lineCap;
	ctx.lineCap="square";
	ctx.beginPath();
	ctx.moveTo(xMin, yMin);
	ctx.lineTo(xMin, yMax);
	ctx.lineTo(xMax, yMax);
	ctx.lineTo(xMax, yMin);
	ctx.lineTo(xMin, yMin);
	ctx.stroke();
	ctx.lineCap=cap;
}

// takes a bounding box in chip coords and centres the display over it
function zoomToBox(xmin,xmax,ymin,ymax)
{
	var xmid=(xmin+xmax)/2;
	var ymid=(ymin+ymax)/2;
	var x=(xmid+400)/grChipSize*600;
	var y=600-ymid/grChipSize*600;
	var zoom=5;  // pending a more careful calculation
	moveHere([x,y,zoom]);
}
/*
* Draws a polygon from a set of coordinates into the specified canvas
*/
function drawSeg(ctx, seg)
{
	ctx.beginPath();
	ctx.moveTo(grScale(seg[0]),grScale(seg[1]));
	for(var i=2;i<seg.length;i+=2)
	{
		ctx.lineTo(grScale(seg[i]),grScale(seg[i+1]));
	}
}

/*
* Decodes a pixel in the hitbuffer into the number of the node there
*/
function findNodeNumber(x,y)
{
	var ctx = hitbuffer.getContext('2d');
	var pixels = ctx.getImageData(x*grCanvasSize/600, y*grCanvasSize/600, 2, 2).data;
	if(pixels[0]==0) return -1;
	var high = pixels[0]>>4;
	var mid = pixels[1]>>4;
	var low = pixels[2]>>4;
	return (high<<8)+(mid<<4)+low;
}

// remove red/white overlay according to logic value
// for easier layout navigation
function clearHighlight()
{
	ctx.clearRect(0,0,grCanvasSize,grCanvasSize);
}
/*
* Called when a layer on/off checkbox is clicked
* Updates which layers are shown then redraws the background
*/
function updateShow(layer, on)
{
	if(layer==-1)
	{
		drawTLayer = on;
	}
	else
	{
		drawlayers[layer]=on;
	}
	setupBackground();
}

/*
* Draws a box label, originally in expertWires.js
*/
function boxLabel(args) {
	var text = args[0];
	var textsize = args[1];
	var thickness = 1+ textsize / 20;
	var boxXmin = args[2] * grCanvasSize / grChipSize;
	var boxYmin = args[3] * grCanvasSize / grChipSize;
	var boxXmax = args[4] * grCanvasSize / grChipSize;
	var boxYmax = args[5] * grCanvasSize / grChipSize;
	ctx.lineWidth   = thickness;
	ctx.font        = textsize + labelFont;
	//TODO: Allow for multiple box label colors based on parameters
	ctx.fillStyle   = boxFill;
	ctx.strokeStyle = boxStroke;
	if(args.length>4){
		ctxDrawBox(ctx, boxXmin, boxYmin, boxXmax, boxYmax);
		// offset the text label to the interior of the box
		boxYmin -= thickness * 2;
	}
	ctx.strokeStyle = labelStroke;  // white
	ctx.lineWidth   = thickness*2;
	ctx.strokeText(text, boxXmin, boxYmin);
	ctx.fillText(text, boxXmin, boxYmin);
}

//Flashes label, also from expertWires.js
function flashBoxLabel(args)
{
	clearHighlight();
	var callBack = function(){boxLabel(args);};
	setTimeout(callBack, 400);
	setTimeout(clearHighlight,  800);
	setTimeout(callBack, 1200);
}
// we draw the chip data scaled down to the canvas
// and so avoid scaling a large canvas
function grScale(x)
{
	return x*grCanvasSize/grChipSize;
}

function localx(el, gx)
{
	return gx-el.getBoundingClientRect().left;
}

function localy(el, gy)
{
	return gy-el.getBoundingClientRect().top;
}


function now(){return  new Date().getTime();}
